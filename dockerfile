FROM debian:stable

RUN apt-get update && apt-get install -y nginx openssh-server

RUN useradd -ms /bin/bash -g ssh my_user

RUN echo 'my_user:lmyzefc*' | chpasswd

RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"

RUN echo "export VISIBLE=now" >> /etc/profile

RUN apt-get clean

RUN rm -rf /var/lib/apt/lists/*

EXPOSE 80 22

# RUN groupadd my_group && useradd -ms /bin/bash -g my_group my_user

CMD service ssh start && nginx -g 'daemon off;'
