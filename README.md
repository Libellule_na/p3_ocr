# projet_3-ED

Environnement de travail local `projet_03` OCR parcours "Expert Devops"

# Installation

cloner le projet
```
git clone https://gitlab.com/Libellule_na/p3_ocr.git

```
se déplacer dans le bon répertoire
```
cd p3_ocr
```
lancement du processus d'installation de vagrant et du container docker
```
vagrant up
```
# Nginx & SSH

nginx accessible via
```
http://0.0.0.0:8080
```
ssh accessible via
```
ssh my_user@0.0.0.0 -p 9090
```
